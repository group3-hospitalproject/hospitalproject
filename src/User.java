class User {
    private String userId;
    private boolean isLoggedIn;

    public User(String userId) {
        this.userId = userId;
        this.isLoggedIn = false;
    }

    public String getUserId() {
        return userId;
    }

    public void login() {
        isLoggedIn = true;
        System.out.println("Login successful!");
    }

    public void logout() {
        isLoggedIn = false;
        System.out.println("Logged out successfully.");
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }
}

