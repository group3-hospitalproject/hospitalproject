import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class HealthcareManagementSystem {
    private List<User> registeredUsers;
    private Scanner scanner;

    public HealthcareManagementSystem() {
        registeredUsers = new ArrayList<>();
        scanner = new Scanner(System.in);
    }

    public void registerNewUser(User newUser) {
        registeredUsers.add(newUser);
    }

    public User accessUserProfile(String userId) {
        for (User user : registeredUsers) {
            if (user.getUserId().equals(userId)) {
                return user;
            }
        }
        return null;
    }

    public void coordinateAppointments(User currentUser) {
        System.out.println("What would you like to do?");
        System.out.println("1. Schedule a new appointment");
        System.out.println("2. Update details of an existing appointment");
        System.out.println("3. View appointment details");
        System.out.println("4. Logout");

        int choice = scanner.nextInt();
        scanner.nextLine(); // Consume newline character

        switch (choice) {
            case 1:
                promptForAppointment(currentUser);
                break;
            case 2:
                promptForUpdate();
                break;
            case 3:
                // Add logic for viewing appointment details
                promptForDetails();
                break;
            case 4:
                currentUser.logout();
                System.out.println("Logged out successfully.");
                break;
            default:
                System.out.println("Invalid choice.");
        }
    }

    private void promptForAppointment(User currentUser) {
        boolean restart = false;

        do {
            System.out.println("Enter patient's user ID: ");
            String patientId = scanner.nextLine();
            User patient = accessUserProfile(patientId);

            if (patient == null) {
                System.out.println("Patient ID not found. Please enter a valid ID or type 'restart' to start over.");
                String input = scanner.nextLine();
                if (input.equalsIgnoreCase("restart")) {
                    restart = true;
                    break;
                }
            } else {
                System.out.println("Enter doctor's user ID: ");
                String doctorId = scanner.nextLine();
                User doctor = accessUserProfile(doctorId);

                if (doctor == null) {
                    System.out.println("Doctor ID not found. Please enter a valid ID or type 'restart' to start over.");
                    String input = scanner.nextLine();
                    if (input.equalsIgnoreCase("restart")) {
                        restart = true;
                        break;
                    }
                } else {
                    System.out.println("Enter appointment time: ");
                    String appointmentTime = scanner.nextLine();

                    // scheduleNewAppointment(patient, doctor, appointmentTime);
                    return;
                }
            }
        } while (restart);

        coordinateAppointments(currentUser); // Restart the coordination process
    }

    private void promptForUpdate() {
        System.out.println("Enter appointment ID to update: ");
        int appointmentId = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Enter new details for the appointment: ");
        String newDetails = scanner.nextLine();

        // viewUpdateAppointmentDetails(appointmentId, newDetails);
    }

    private void promptForDetails() {
        System.out.println("Enter appointment ID to view: ");
        int appointmentId = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Enter new details for the appointment: ");
        String newDetails = "";

        // viewUpdateAppointmentDetails(appointmentId, newDetails);
    }
}
