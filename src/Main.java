import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        HealthcareManagementSystem healthcareSystem = new HealthcareManagementSystem();
        Scanner scanner = new Scanner(System.in);
        User currentUser = null;

        while (true) {
            System.out.println("Choose an option:");
            System.out.println("1. Register");
            System.out.println("2. Login");
            System.out.println("3. Exit");

            int choice = scanner.nextInt();
            scanner.nextLine(); // Consume newline character

            switch (choice) {
                case 1:
                    // TO DO: Implement user registration logic
                    User newUser = new User("test");
                    healthcareSystem.registerNewUser(newUser);
                    break;
                case 2:
                    System.out.println("Enter your user ID: ");
                    String userId = scanner.nextLine();
                    currentUser = healthcareSystem.accessUserProfile(userId);

                    if (currentUser != null) {
                        currentUser.login();
                        if (currentUser.isLoggedIn()) {
                            healthcareSystem.coordinateAppointments(currentUser);
                        } else {
                            System.out.println("Login failed. Try again.");
                        }
                    } else {
                        System.out.println("User not found. Please register.");
                    }
                    break;
                case 3:
                    System.out.println("Exiting...");
                    return;
                default:
                    System.out.println("Invalid choice.");
            }
        }
    }
}